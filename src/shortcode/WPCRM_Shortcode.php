<?php
namespace MILEXA\WPAWESOME\ADDONS\WPCRM;

if ( ! class_exists("MILEXA\\WPAWESOME\\ADDONS\\WPCRM\\WPCRM_Shortcode") ) :
class WPCRM_Shortcode
{
    public static function init()
    {
        $class = __CLASS__;
        new $class;
    }

    public function __construct(){

    }

}
endif;