<?php
namespace MILEXA\WPAWESOME\ADDONS\WPCRM;
if ( ! class_exists("MILEXA\\WPAWESOME\\ADDONS\\WPCRM\\WPCRM_Menu") ) :
class WPCRM_Menu
{
    public static function load_menu(){

    }

    /**
     * Remove the first menu from the backend sidebar
     */
    public static function removeFirstMenu() {
        global $submenu;
        if($submenu['edit.php?post_type=app_manager']){
            unset($submenu['edit.php?post_type=app_manager'][5]);
            unset($submenu['edit.php?post_type=app_manager'][10]);
            $submenu['edit.php?post_type=app_manager'][12]   =   [
                "Apps Manager",
                "edit_posts",
                "edit.php?post_type=app_manager"
            ];
            $submenu['edit.php?post_type=app_manager'][13]   =  $submenu['edit.php?post_type=app_manager'][11];
            unset($submenu['edit.php?post_type=app_manager'][11]);
        }
    }
}
endif;
