<?php
namespace MILEXA\WPAWESOME\ADDONS\WPCRM;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

if ( ! class_exists("MILEXA\\WPAWESOME\\ADDONS\\WPCRM\\WPCRM_Sys") ) :
    class WPCRM_Sys {
        public function __construct(){}

        public function CreateAppFolders($app){
            $adapter    = new Local(AA_PATH.'/apps', LOCK_EX, Local::SKIP_LINKS);
            $filesystem = new Filesystem($adapter);
            $newApp     = ucfirst($app);
            $filesystem->write("{$app}/config/{$newApp}Config.php", $this->createConfigFile($app));
            $filesystem->write("{$app}/frontend/{$newApp}FrontendView.php", $this->createFrontendFile($app));
            $filesystem->write("{$app}/menu/{$newApp}Menu.php", $this->createMenuFile($app));
            $filesystem->write("{$app}/models/.gitignore", "");
            $filesystem->write("{$app}/shortcode/{$newApp}shortcode.php", $this->createShortCodeFile($app));
            $filesystem->write("{$app}/views/{$app}/objectManager.php", $this->createViewsFile());
            $filesystem->write("{$app}/{$newApp}App.php", $this->createAppFile($app));
            $filesystem->write("{$app}/setting.json", $this->createSettingFile($app));
            $filesystem->write("{$app}/logo.svg", "");

            $this->updateComposerFile($app);
        }

        /**
         * @param $app
         */
        protected function updateComposerFile($app){
            $adapter            = new Local(AA_PATH, LOCK_EX, Local::SKIP_LINKS);
            $filesystem         = new Filesystem($adapter);
            $json               = $filesystem->read('composer.json');
            $contentsDecoded    = json_decode($json, true);
            $contentsDecoded['autoload']['psr-4']["APPS\\".strtoupper($app)."\\"]     = [
                "apps/".strtolower($app),
                "apps/".strtolower($app)."/config",
                "apps/".strtolower($app)."/frontend",
                "apps/".strtolower($app)."/menu",
                "apps/".strtolower($app)."/shortcode"
            ];
            $json = json_encode($contentsDecoded, JSON_UNESCAPED_SLASHES, JSON_PRETTY_PRINT);
            $filesystem->update('composer.json', $json);

            exec("cd ".AA_PATH.";php composer dump-autoload");
        }

        /**
         * @param $app
         * @return bool|false|mixed|string
         */
        public function createConfigFile($app){
            $adapter    = new Local(__DIR__.'/../dummy', LOCK_EX, Local::SKIP_LINKS);
            $filesystem = new Filesystem($adapter);
            $contents   = $filesystem->read('config.txt');
            $contents   = str_replace([
                "{APP_NAME}",
                "{APP_FILE_NAME}",
                "{OBJECT_NAME}",
            ], [
                strtoupper($app),
                ucfirst($app),
                strtolower($app)
            ],$contents);
            return $contents;
        }

        /**
         * @param $app
         * @return bool|false|mixed|string
         */
        protected function createFrontendFile($app){
            $adapter    = new Local(__DIR__.'/../dummy', LOCK_EX, Local::SKIP_LINKS);
            $filesystem = new Filesystem($adapter);
            $contents   = $filesystem->read('frontend.txt');
            $contents   = str_replace([
                "{APP_NAME}",
                "{APP_FILE_NAME}"
            ], [
                strtoupper($app),
                ucfirst($app)
            ],$contents);
            return $contents;
        }

        /**
         * @param $app
         * @return bool|false|mixed|string
         */
        protected function createMenuFile($app){
            $adapter    = new Local(__DIR__.'/../dummy', LOCK_EX, Local::SKIP_LINKS);
            $filesystem = new Filesystem($adapter);
            $contents   = $filesystem->read('menu.txt');
            $contents   = str_replace([
                "{APP_NAME}",
                "{APP_FILE_NAME}",
                "{OBJECT_NAME}",
            ], [
                strtoupper($app),
                ucfirst($app),
                strtolower($app)
            ],$contents);
            return $contents;
        }

        /**
         * @param $app
         * @return bool|false|mixed|string
         */
        protected function createShortCodeFile($app){
            $adapter    = new Local(__DIR__.'/../dummy', LOCK_EX, Local::SKIP_LINKS);
            $filesystem = new Filesystem($adapter);
            $contents   = $filesystem->read('shortcode.txt');
            $contents   = str_replace([
                "{APP_NAME}",
                "{APP_FILE_NAME}"
            ], [
                strtoupper($app),
                ucfirst($app)
            ],$contents);
            return $contents;
        }

        /**
         * @param $app
         * @return bool|false|string
         */
        protected function createViewsFile(){
            $adapter    = new Local(__DIR__.'/../dummy', LOCK_EX, Local::SKIP_LINKS);
            $filesystem = new Filesystem($adapter);
            $contents   = $filesystem->read('view.txt');
            return $contents;
        }

        /**
         * @param $app
         * @return bool|false|mixed|string
         */
        protected function createAppFile($app){
            $adapter    = new Local(__DIR__.'/../dummy', LOCK_EX, Local::SKIP_LINKS);
            $filesystem = new Filesystem($adapter);
            $contents   = $filesystem->read('app.txt');
            $contents   = str_replace([
                "{APP_NAME}",
                "{APP_FILE_NAME}",
                "{OBJECT_NAME}",
            ], [
                strtoupper($app),
                ucfirst($app),
                strtolower($app)
            ],$contents);
            return $contents;
        }

        /**
         * @param $app
         * @return bool|false|mixed|string
         */
        protected function createSettingFile($app){
            $adapter    = new Local(__DIR__.'/../dummy', LOCK_EX, Local::SKIP_LINKS);
            $filesystem = new Filesystem($adapter);
            $contents   = $filesystem->read('setting.txt');
            $contents   = str_replace([
                "{APP_NAME}",
                "{APP_FILE_NAME}"
            ], [
                strtoupper($app),
                ucfirst($app)
            ],$contents);
            return $contents;
        }

        public function checkIfAppExist($app){
            $adapter            = new Local(AA_PATH.'/apps', LOCK_EX, Local::SKIP_LINKS);
            $filesystem         = new Filesystem($adapter);
            $exists             = $filesystem->has("{$app}/setting.json");
            return $exists;
        }

        public function deleteApp($app){
            $adapter            = new Local(AA_PATH.'/apps', LOCK_EX, Local::SKIP_LINKS);
            $filesystem         = new Filesystem($adapter);
            $filesystem->deleteDir($app);
        }

        public function deleteObjects($app, $object_name){
            $adapter            = new Local(AA_PATH."/apps/{$app}", LOCK_EX, Local::SKIP_LINKS);
            $filesystem         = new Filesystem($adapter);
            $filesystem->deleteDir("views/{$object_name}");
        }

        public function CreateObjectViews($app, $object_name){
            $adapter            = new Local(AA_PATH."/apps/{$app}", LOCK_EX, Local::SKIP_LINKS);
            $filesystem         = new Filesystem($adapter);
            $filesystem->write("views/{$object_name}/index.php", $this->createObjectViewFile($app, $object_name));
        }

        public function checkIfObjectExist($app, $object_name){
            $adapter            = new Local(AA_PATH."/apps/{$app}", LOCK_EX, Local::SKIP_LINKS);
            $filesystem         = new Filesystem($adapter);
            $exists             = $filesystem->has("views/{$object_name}/index.php");
            return $exists;
        }

        /**
         * @param $object_name
         * @return bool|false|mixed|string
         */
        protected function createObjectViewFile($app, $object_name){
            $adapter    = new Local(__DIR__.'/../dummy/objects', LOCK_EX, Local::SKIP_LINKS);
            $filesystem = new Filesystem($adapter);
            $contents   = $filesystem->read('object_list_view.txt');
            $contents   = str_replace([
                "{APP_FILE_NAME}",
                "{OBJECT_NAME}",
                "{APP_NAME}",
                "{APP_CLASS}",
            ], [
                ucfirst($object_name),
                strtolower($object_name),
                strtoupper($app),
                ucfirst($app)
            ],$contents);
            return $contents;
        }
    }
endif;
