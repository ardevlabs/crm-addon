<?php
namespace APPS\{APP_NAME};
use MILEXA\WPAWESOME\WC_MWViews;

if ( ! class_exists( 'APPS\\{APP_NAME}\\{APP_FILE_NAME}App') ) :
class {APP_FILE_NAME}App
{
	/**
	 *
	 */
	public static function init(){
        $class = __CLASS__;
        new $class;
    }

	/**
	 * {APP_FILE_NAME}App constructor.
	 */
	public function __construct(){}

    public static function generateView(){
         $page = $_GET['page'];
         if($page):
             $view = new WC_MWViews();
             $tpl = [
                 "view"      => "{$page}.index",
                 "addon"     => strtolower("{APP_NAME}")
             ];
             $view->makeAppView($tpl);
         else:
             wp_die("Menu view not found");
         endif;
    }

    public static function objectManagerSetting(){
        $view = new WC_MWViews();
        $tpl = [
            "view"      => "{OBJECT_NAME}.objectManager",
            "addon"     => "{OBJECT_NAME}"
        ];
        $view->makeAppView($tpl);
    }
}
endif;