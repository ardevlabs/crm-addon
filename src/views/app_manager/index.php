<style>
    #ob_setting > .inside {
        margin: 0;
        padding: 0;
    }
    #ob_setting > .handlediv, #ob_setting > h2.ui-sortable-handle {
        display: none;
    }
</style>
<div id="contextual-help-wrap" class="hidden" tabindex="-1" aria-label="Contextual Help Tab" style="display: block;">
    <div id="contextual-help-back"></div>
    <div id="contextual-help-columns">
        <div class="contextual-help-tabs">
            <ul>
                <li class="active">
                    <a href="#tab-details" aria-controls="tab-details">Details</a>
                </li>
                <li>
                    <a href="#tab-fields" aria-controls="tab-fields">Fields</a>
                </li>
                <li>
                    <a href="#tab-records" aria-controls="tab-records">Records</a>
                </li>
            </ul>
        </div>

        <div class="contextual-help-sidebar">
            <p><strong>For more information:</strong></p>
            <p><a href="#">Documentation</a></p>
            <p><a href="#">Support</a></p>
        </div>

        <div class="contextual-help-tabs-wrap">

            <div id="tab-details" class="help-tab-content active">
                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                <p>You must click the Save Changes button at the bottom of the screen for new settings to take effect.</p>
            </div>

            <div id="tab-fields" class="help-tab-content" style="display: none;">
                <p>Post via email settings allow you to send your WordPress installation an email with the content of your post. You must set up a secret email account with POP3 access to use this, and any mail received at this address will be posted, so it’s a good idea to keep this address very secret.</p>
            </div>

            <div id="tab-records" class="help-tab-content" style="display: none">
                <p>If desired, WordPress will automatically alert various services of your new posts.</p>
            </div>
        </div>
    </div>
</div>
