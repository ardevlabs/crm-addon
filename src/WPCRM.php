<?php
namespace MILEXA\WPAWESOME\ADDONS\WPCRM;
use MILEXA\WPAWESOME\WC_MWViews;

if ( ! class_exists("MILEXA\\WPAWESOME\\ADDONS\\WPCRM\\WPCRM") ) :
    class WPCRM
    {
        /**
         *
         */
        public static function init(){
            $class = __CLASS__;
            new $class;
        }

        /**
         * WPCRM constructor.
         */
        public function __construct(){}

        public static function apptManagerSetting(){
            $view = new WC_MWViews();
            $tpl = [
                "view"      => "app_manager.index",
                "addon"     => "crm-addon"
            ];
            $view->makeAddonView($tpl);
        }

    }
endif;
