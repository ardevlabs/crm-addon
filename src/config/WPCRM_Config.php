<?php
namespace MILEXA\WPAWESOME\ADDONS\WPCRM;

if ( ! class_exists("MILEXA\\WPAWESOME\\ADDONS\\WPCRM\\WPCRM_Config") ) :
    class WPCRM_Config
    {
        public static function init(){
            $class = __CLASS__;
            new $class;
        }
        public function __construct(){
            add_action('save_post',                              [$this, 'saveApp'], 10, 3);
            add_action('before_delete_post',                     [$this, 'deleteApp'], 10, 1 );
            add_action('init',                                   [$this, 'appManagerPostType']);
            add_action('add_meta_boxes',                         [$this, 'app_manager_metabox']);
            add_action("manage_app_manager_posts_custom_column", [$this, "appManagerColumns"] ,10,3);
            add_filter('manage_edit-app_manager_columns',        [$this, 'app_manager_column']);
            add_action('admin_enqueue_scripts',                  [$this, 'enqueue_scripts']);
        }


        /**
         * @param $post_id
         * @param $post
         * @param $update
         */
        public function saveApp($post_id, $post, $update){
            if (($post->post_status != 'publish') || ($post->post_type != 'app_manager')) {
                return;
            }

            $new_app    = new WPCRM_Sys();
            $app_exist  = $new_app->checkIfAppExist($post->post_name);
            if(!$app_exist):
                $new_app->CreateAppFolders($post->post_name);
            endif;
        }

        /**
         * @param $post_id
         */
        public function deleteApp($post_id){
            if(get_post_type( $post_id ) == 'app_manager'):
                $new_app    = new WPCRM_Sys();
                $posts      = get_post($post_id);
                $new_app->deleteApp(str_replace("__trashed", "", $posts->post_name));
            endif;
        }
        public function appManagerPostType() {
            $labels = [
                'name'               => _x( 'Apps Manager', '' ),
                'singular_name'      => _x( 'App Manager', '' ),
                'add_new'            => _x( 'Add App', '' ),
                'add_new_item'       => __( 'Add New App' ),
                'edit_item'          => __( 'Edit App' ),
                'new_item'           => __( 'New App' ),
                'all_items'          => __( 'Apps' ),
                'view_item'          => __( 'View App' ),
                'search_items'       => __( 'Search Apps' ),
                'not_found'          => __( 'No Apps found' ),
                'not_found_in_trash' => __( 'No Apps found in the Trash' ),
                'featured_image'     => __( 'App logo', '' ),
                'parent_item_colon'  => '',
                'menu_name'          => 'AR CRM',
            ];
            $args = [
                'labels'                => $labels,
                'description'           => '',
                'menu_position'         => 65,
                'menu_icon'             => AA_URL.'logo.svg',
                'supports'              => ['title', 'editor','thumbnail'],
                'show_in_admin_bar'     => true,
                'show_in_menu'          => true,
                'public'                => false,  // it's not public, it shouldn't have it's own permalink, and so on
                'publicly_queriable'    => true,  // you should be able to query it
                'show_ui'               => true,  // you should be able to edit it in wp-admin
                'exclude_from_search'   => true,  // you should exclude it from search results
                'show_in_nav_menus'     => false,  // you shouldn't be able to add it to menus
                'has_archive'           => false,  // it shouldn't have archive page
                'rewrite'               => false,  // it shouldn't have rewrite rules
                'capabilities'      => [
                    'create_posts'  => true
                ],
                'map_meta_cap'      => true
            ];

            register_post_type('app_manager', $args );
        }

        /**
         * @param $column
         */
        public function appManagerColumns($column){
            global $post;
            switch ($column) {
                case 'title':
                    echo $post->post_title;
                    break;
                case 'developer':
                    echo get_the_author_meta('display_name', $post->post_author);
                    break;
                case 'description':
                    echo $post->post_content;
                    break;
                case 'logo':
                    echo "<div class='app-logo'>".get_the_post_thumbnail( $post, [32,32])."</div>";
                    break;
                case 'last_modified':
                    echo 'Updated';
                    echo '<br>';
                    echo '<abbr title="'.$post->post_modified.'">'.human_time_diff( strtotime($post->post_modified), current_time('timestamp') ).' ago</abbr>';
                    break;
                default:break;
            }
        }

        /**
         * @param $columns
         * @return mixed
         */
        public function app_manager_column($columns){
            unset($columns);
            $columns['cb']            = '<input type="checkbox" />';
            $columns['logo']          = 'APP LOGO';
            $columns['title']         = 'APP NAME';
            $columns['developer']     = 'DEVELOPER NAME';
            $columns['description']   = 'DESCRIPTION';
            $columns['last_modified'] = 'LAST MODIFIED DATE';

            return $columns;
        }
        public function app_manager_metabox(){
            global $post;
            add_meta_box('ob_setting', __('Object Manager', ''), ["MILEXA\\WPAWESOME\\ADDONS\\WPCRM\\WPCRM", 'apptManagerSetting'], 'object_manager', 'normal', 'core');
        }
        public function enqueue_scripts(){
            wp_enqueue_style('ar-app-style', AA_URL.'assets/css/app.css');

            //Script
            wp_enqueue_script('ar-app-script', AA_URL.'assets/js/app.js', false, null,true);
        }
    }
endif;
